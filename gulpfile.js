var gulp = require("gulp");
var concat = require("gulp-concat");
var connect = require("gulp-connect");
var templateCache = require('gulp-angular-templatecache');
var addsrc = require('gulp-add-src');

gulp.task("default", [
    "build-css",
    "build-tpls",
    "watch",
    "server"
]);

gulp.task("build-css", function () {
    var src = [
        "src/**/*.css"
    ];

    gulp.src(src)
        .pipe(concat("stcontrols.css"))
        .pipe(gulp.dest("./dist"))
});

gulp.task('build-tpls', function () {
    var options = {
        module: 'stcontrols',
        standalone: false
    };

    return gulp.src('./src/**/*.html')
        .pipe(templateCache('./templates.js', options))
        .pipe(addsrc("./dist/stcontrols.js"))
        .pipe(concat("stcontrols.tpls.js"))
        .pipe(gulp.dest("./dist"));
});

gulp.task("watch", function () {
    gulp.watch("./src/**/*.css", ["build-css"]);
    gulp.watch(["./src/**/*.html", "./dist/stcontrols.js"], ["build-tpls"]);
});

gulp.task("server", function () {
    return connect.server({
        name: "demo",
        root: "./",
        port: "9001",
        https: false
    });
});