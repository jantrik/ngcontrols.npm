/// <reference path="../stcontrols.module.ts" />

namespace stcontrols {
    export class EventSource {
        private _handlers: {};
        constructor() {
            this._handlers = {};
        }
        on(eventName: string, handler: (event) => any): EventSource {
            let handlerList: [any] = this._getHandlerList(eventName);
            handlerList.push(handler);
            return this;
        }
        onAll(eventNames: string[], handler: (event) => any): EventSource {
            eventNames.forEach((eventName) => this.on(eventName, handler));
            return this;
        }
        fire(eventName: string, event: any): EventSource {
            let handlerList: [any] = this._getHandlerList(eventName);
            handlerList.forEach(handler => handler.call(this, event));
            return this;
        }
        _getHandlerList(eventName: string): [any] {
            eventName = (eventName || "").toLowerCase();
            if (!this._handlers[eventName]) {
                this._handlers[eventName] = [];
            }

            return this._handlers[eventName];
        }
    }

    stcontrols.module.value("EventSource", EventSource);
}

