namespace stcontrols {
    export interface JstreeNodeFilter {
        (node: JstreeNode, libNode, jstreeInstance: JSTree): boolean;
    }
}