/// <reference path="../stcontrols.module.ts" />

namespace stcontrols {
    stcontrols.module
        .directive("jstreeDropdown", [
            () => {
                return {
                    templateUrl: "jstree/jstree-dropdown.directive.html",
                    scope: {
                        model: "="
                    },
                    link: (scope) => {
                        let model: JstreeModel = scope.model;
                        scope.selectionText = "";
                        
                        function updateSelectionText() {
                            let selectedItems = model.getSelected();
                            let selectedNames = model.getNames(selectedItems);

                            if (selectedNames.length > 0) {
                                scope.selectionText = selectedNames[0];
                                if (selectedNames.length > 1) {
                                    scope.selectionText += " and " + (selectedNames.length - 1) + " more";
                                }
                            } else {
                                scope.selectionText = model._options.label || "no items selected";
                            }
                        }

                        updateSelectionText();
                        model.on("setSelected", () => updateSelectionText());
                    }
                };
            }
        ]);
}