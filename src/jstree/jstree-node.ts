/// <reference path="../stcontrols.module.ts" />

namespace stcontrols {
    export class JstreeNode {
        isRoot: boolean;
        isLeaf: boolean;
        level: number;
        parentNode: JstreeNode;
        text: any;
        parentId: any;
        id: any;
        children: JstreeNode[];
        constructor(
            public item: any
            , idProp: string
            , parentIdProp: string
            , nameProp: string) {
            this.item = item;
            this.id = item[idProp];
            this.parentId = item[parentIdProp];
            this.text = item[nameProp];
            this.children = [];
        }
        _buildIndex(options: JstreeOptions) {
            this.level = this.parentNode ? this.parentNode.level + 1 : 0;
            this.isRoot = this.level == 0;
            this.isLeaf = this.children.length == 0;
            if (options.sortBy) {
                this.children = _.sortBy(this.children, "item." + options.sortBy);
            }
            this.children.forEach(child => {
                child.parentNode = this;
                child._buildIndex(options);
            });
        }
    }

    stcontrols.module.value("JstreeNode", JstreeNode);
}