/// <reference path="../stcontrols.module.ts" />

namespace stcontrols {

    stcontrols.module
        .directive("jstree", [
            "$q",
            "$timeout",
            ($q, $timeout) => {
                return {
                    templateUrl: "jstree/jstree.directive.html",
                    scope: {
                        model: "="
                    },
                    link: function (scope, element, attrs): void {
                        scope.searchText = "";
                        scope.search = () => instance.search(scope.searchText);

                        let $tree;
                        let instance: JSTree;
                        let defer: angular.IDeferred<any>;
                        let $treeContainer = element.find(".tree-container");

                        let model: JstreeModel = scope.model;

                        function getSelected() {
                            let selectedNodes = instance.get_selected(true);
                            let filtered = _.filter(selectedNodes,
                                node => model._selectionFilter(node.original, node, instance)
                            );
                            return _.map(filtered, 'id');
                        }

                        function ensure(action) {
                            if (defer) {
                                defer.promise.then(action);
                            }
                        }

                        function initTree() {
                            if ($tree) {
                                $tree.remove();
                            }
                            defer = $q.defer();

                            $tree = $("<div>").appendTo($treeContainer)
                                .bind('loaded.jstree', function () {
                                    instance = $tree.jstree(true);
                                    setSelectedFromModel();
                                    setDisabledFromModel();
                                    setHiddenFromModel();
                                    defer.resolve();
                                }).bind('changed.jstree', function () {
                                    let selectedIds = getSelected();
                                    model.setSelectedIds(selectedIds, true);
                                    $timeout();
                                });

                            let treeOptions: any = {
                                core: {
                                    data: model._fakeRoot.children,
                                    check_callback: true,
                                    multiple: !!model._options.multiple,
                                    expand_selected_onload: false
                                },
                                plugins: [
                                    "search"
                                    , "conditionalselect"
                                ],
                                search: {
                                    show_only_matches: true,
                                    show_only_matches_children: true
                                },
                                conditionalselect: (node, event) => model._checkFilter(node.original, node, instance)
                            };

                            if (model._options.multiple) {
                                treeOptions.plugins.push('checkbox');
                                treeOptions.checkbox = {
                                    visible: true,
                                    whole_node: true,
                                    tie_selection: true,
                                    keep_selected_style: false
                                };
                            }

                            $tree.jstree(treeOptions);
                        }

                        function setSelectedFromModel() {
                            if (instance) {
                                instance.deselect_all(true);
                                instance.select_node(model.getSelectedIds(), true, !!model._options.collapseSelected);
                            }
                        }

                        function setDisabledFromModel() {
                            if (instance) {
                                instance.disable_node(model.getDisabledIds());
                            }
                        }

                        function setHiddenFromModel() {
                            if (instance) {
                                instance.show_all(false);
                                instance.hide_node(model.getHiddenIds(), false);
                            }
                        }

                        model.onAll(["setItems", "setOptions"], event => initTree())
                            .on("setSelected", function (event) {
                                if (!event.fromView) {
                                    ensure(setSelectedFromModel);
                                }
                            }).on("setDisabled", event => {
                                ensure(setDisabledFromModel);
                            }).on("setHidden", event => ensure(setHiddenFromModel));
                    }
                };
            }
        ]);
}

