/// <reference path="../stcontrols.module.ts" />
/// <reference path="jstree-node.ts" />
/// <reference path="jstree-node-filter.ts" />
/// <reference path="../common/event-source.ts" />

namespace stcontrols {
    export class JstreeModel extends EventSource {
        _hiddenIds: any[];
        _checkFilter: JstreeNodeFilter;
        _selectionFilter: JstreeNodeFilter;
        _itemsIndex: any;
        _nodesIndex: any;
        _nodes: JstreeNode[];
        _fakeRoot: JstreeNode;
        _disabledIds: any[];
        _selectedIds: any[];
        _items: [any];
        constructor(private _idProp: string,
            private _parentIdProp: string,
            private _nameProp: string,
            public _options: JstreeOptions) {
            super();
            this._selectedIds = [];
            this._disabledIds = [];
            this._selectionFilter = () => true;
            this._checkFilter = () => true;
            this._nodes = [];
            this._nodesIndex = {};
            this._itemsIndex = {};
        }
        setItems(items: [any], fromView: boolean) {
            this._items = items;
            this._fakeRoot = new JstreeNode({}, this._idProp, this._parentIdProp, this._nameProp);
            this._nodes = _.map(items, (item) => new JstreeNode(item, this._idProp, this._parentIdProp, this._nameProp));
            this._nodesIndex = _.keyBy(this._nodes, "id");
            this._itemsIndex = _.keyBy(this._items, this._idProp);
            _.forEach(this._nodes, (node) => {
                let parent: any = this._nodesIndex[node.parentId] || this._fakeRoot;
                parent.children.push(node);
            });
            this._fakeRoot._buildIndex(this._options);

            this.fire("setItems", {
                data: items,
                fromView: fromView
            });
            return this;
        }
        getItemsById(ids: any[]) {
            return _.map(ids, id => this._itemsIndex[id]);
        }
        setSelectedIds(ids: any[], fromView: boolean) {
            this._selectedIds = ids;
            this.fire("setSelected", {
                data: ids,
                fromView: fromView
            });
            return this;
        }
        getSelectedIds() {
            return this._selectedIds;
        }
        getSelected() {
            var items = this.getItemsById(this._selectedIds);
            return items;
        }
        getFirstSelected() {
            return this.getSelected()[0];
        }
        setHiddenIds(ids: any[], fromView: boolean) {
            this._hiddenIds = ids;
            this.fire("setHidden", {
                data: ids,
                fromView: fromView
            });
            return this;
        }
        getHiddenIds() {
            return this._hiddenIds;
        }
        setDisabledIds(ids: any[], fromView: boolean) {
            this._disabledIds = ids;
            this.fire("setDisabled", {
                data: ids,
                fromView: fromView
            });
            return this;
        }
        getDisabledIds() {
            return this._disabledIds;
        }
        getNames(items: any[]) {
            return _.map(items, this._nameProp);
        }
        getIds(items: any[]) {
            return _.map(items, this._idProp);
        }
        setOptions(options: any) {
            this._options = options;
            this._fakeRoot && this._fakeRoot._buildIndex(this._options);
            this.fire("setOptions", {
                data: options
            });
            return this;
        }
        setSelectionFilter(selectionFilter: JstreeNodeFilter) {
            this._selectionFilter = selectionFilter;
            if (this._selectionFilter == null) {
                this._selectionFilter = () => true;
            }
            if (this._options.multiple) {
                this._checkFilter = () => true;
            } else {
                this._checkFilter = this._selectionFilter;
            }
            return this;
        }
        getIdsByLevel(level: Number) {
            return _(this._nodes).filter(node => node.level === level).map("id").value();
        }
    }

    stcontrols.module.value("JstreeModel", JstreeModel);
}