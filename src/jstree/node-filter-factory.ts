/// <reference path="../stcontrols.module.ts" />

namespace stcontrols {

    class NodeFilterFactory {
        isLeaf(): JstreeNodeFilter {
            return (node: JstreeNode) => node.isLeaf;
        }
        isNotLeaf(): JstreeNodeFilter {
            return (node: JstreeNode) => !node.isLeaf;
        }
        parentNotSelected(): JstreeNodeFilter {
            return (node: JstreeNode, libNode, jstreeInstance: JSTree) => {
                var parentLibNode = jstreeInstance.get_node(libNode.parent);
                if (!parentLibNode) {
                    return true;
                }
                return !parentLibNode.state.selected;
            };
        }
        isInLevel(level: Number): JstreeNodeFilter {
            return (node: JstreeNode) => node.level == level;
        }
        hasId(ids: any[]): JstreeNodeFilter {
            let idIndex = _.keyBy(ids, id => id);

            return (node) => idIndex[node.id] !== undefined;
        }
        trueForAll(filters: JstreeNodeFilter[]) {
            return (node, libNode, jstreeInstance) => {
                let keep = true;
                for (var i = 0; i < filters.length; i++) {
                    var filter = filters[i];
                    keep = filter(node, libNode, jstreeInstance);

                    if (!keep) break;
                }

                return keep;
            };

        }
    }

    export let nodeFilterFactory = new NodeFilterFactory();

    stcontrols.module.value("nodeFilterFactory", nodeFilterFactory);
}