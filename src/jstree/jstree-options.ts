namespace stcontrols {
    export interface JstreeOptions {
        multiple: boolean;
        sortBy: string;
        label: string;
        collapseSelected: boolean;
    }
}