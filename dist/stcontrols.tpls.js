var stcontrols;
(function (stcontrols) {
    stcontrols.module = angular.module("stcontrols", [
        "ui.bootstrap"
    ]);
})(stcontrols || (stcontrols = {}));
var stcontrols;
(function (stcontrols) {
    var EventSource = (function () {
        function EventSource() {
            this._handlers = {};
        }
        EventSource.prototype.on = function (eventName, handler) {
            var handlerList = this._getHandlerList(eventName);
            handlerList.push(handler);
            return this;
        };
        EventSource.prototype.onAll = function (eventNames, handler) {
            var _this = this;
            eventNames.forEach(function (eventName) { return _this.on(eventName, handler); });
            return this;
        };
        EventSource.prototype.fire = function (eventName, event) {
            var _this = this;
            var handlerList = this._getHandlerList(eventName);
            handlerList.forEach(function (handler) { return handler.call(_this, event); });
            return this;
        };
        EventSource.prototype._getHandlerList = function (eventName) {
            eventName = (eventName || "").toLowerCase();
            if (!this._handlers[eventName]) {
                this._handlers[eventName] = [];
            }
            return this._handlers[eventName];
        };
        return EventSource;
    }());
    stcontrols.EventSource = EventSource;
    stcontrols.module.value("EventSource", EventSource);
})(stcontrols || (stcontrols = {}));
var stcontrols;
(function (stcontrols) {
    stcontrols.module
        .directive("jstreeDropdown", [
        function () {
            return {
                templateUrl: "jstree/jstree-dropdown.directive.html",
                scope: {
                    model: "="
                },
                link: function (scope) {
                    var model = scope.model;
                    scope.selectionText = "";
                    function updateSelectionText() {
                        var selectedItems = model.getSelected();
                        var selectedNames = model.getNames(selectedItems);
                        if (selectedNames.length > 0) {
                            scope.selectionText = selectedNames[0];
                            if (selectedNames.length > 1) {
                                scope.selectionText += " and " + (selectedNames.length - 1) + " more";
                            }
                        }
                        else {
                            scope.selectionText = model._options.label || "no items selected";
                        }
                    }
                    updateSelectionText();
                    model.on("setSelected", function () { return updateSelectionText(); });
                }
            };
        }
    ]);
})(stcontrols || (stcontrols = {}));
var stcontrols;
(function (stcontrols) {
    var JstreeNode = (function () {
        function JstreeNode(item, idProp, parentIdProp, nameProp) {
            this.item = item;
            this.item = item;
            this.id = item[idProp];
            this.parentId = item[parentIdProp];
            this.text = item[nameProp];
            this.children = [];
        }
        JstreeNode.prototype._buildIndex = function (options) {
            var _this = this;
            this.level = this.parentNode ? this.parentNode.level + 1 : 0;
            this.isRoot = this.level == 0;
            this.isLeaf = this.children.length == 0;
            if (options.sortBy) {
                this.children = _.sortBy(this.children, "item." + options.sortBy);
            }
            this.children.forEach(function (child) {
                child.parentNode = _this;
                child._buildIndex(options);
            });
        };
        return JstreeNode;
    }());
    stcontrols.JstreeNode = JstreeNode;
    stcontrols.module.value("JstreeNode", JstreeNode);
})(stcontrols || (stcontrols = {}));
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var stcontrols;
(function (stcontrols) {
    var JstreeModel = (function (_super) {
        __extends(JstreeModel, _super);
        function JstreeModel(_idProp, _parentIdProp, _nameProp, _options) {
            var _this = _super.call(this) || this;
            _this._idProp = _idProp;
            _this._parentIdProp = _parentIdProp;
            _this._nameProp = _nameProp;
            _this._options = _options;
            _this._selectedIds = [];
            _this._disabledIds = [];
            _this._selectionFilter = function () { return true; };
            _this._checkFilter = function () { return true; };
            _this._nodes = [];
            _this._nodesIndex = {};
            _this._itemsIndex = {};
            return _this;
        }
        JstreeModel.prototype.setItems = function (items, fromView) {
            var _this = this;
            this._items = items;
            this._fakeRoot = new stcontrols.JstreeNode({}, this._idProp, this._parentIdProp, this._nameProp);
            this._nodes = _.map(items, function (item) { return new stcontrols.JstreeNode(item, _this._idProp, _this._parentIdProp, _this._nameProp); });
            this._nodesIndex = _.keyBy(this._nodes, "id");
            this._itemsIndex = _.keyBy(this._items, this._idProp);
            _.forEach(this._nodes, function (node) {
                var parent = _this._nodesIndex[node.parentId] || _this._fakeRoot;
                parent.children.push(node);
            });
            this._fakeRoot._buildIndex(this._options);
            this.fire("setItems", {
                data: items,
                fromView: fromView
            });
            return this;
        };
        JstreeModel.prototype.getItemsById = function (ids) {
            var _this = this;
            return _.map(ids, function (id) { return _this._itemsIndex[id]; });
        };
        JstreeModel.prototype.setSelectedIds = function (ids, fromView) {
            this._selectedIds = ids;
            this.fire("setSelected", {
                data: ids,
                fromView: fromView
            });
            return this;
        };
        JstreeModel.prototype.getSelectedIds = function () {
            return this._selectedIds;
        };
        JstreeModel.prototype.getSelected = function () {
            var items = this.getItemsById(this._selectedIds);
            return items;
        };
        JstreeModel.prototype.getFirstSelected = function () {
            return this.getSelected()[0];
        };
        JstreeModel.prototype.setHiddenIds = function (ids, fromView) {
            this._hiddenIds = ids;
            this.fire("setHidden", {
                data: ids,
                fromView: fromView
            });
            return this;
        };
        JstreeModel.prototype.getHiddenIds = function () {
            return this._hiddenIds;
        };
        JstreeModel.prototype.setDisabledIds = function (ids, fromView) {
            this._disabledIds = ids;
            this.fire("setDisabled", {
                data: ids,
                fromView: fromView
            });
            return this;
        };
        JstreeModel.prototype.getDisabledIds = function () {
            return this._disabledIds;
        };
        JstreeModel.prototype.getNames = function (items) {
            return _.map(items, this._nameProp);
        };
        JstreeModel.prototype.getIds = function (items) {
            return _.map(items, this._idProp);
        };
        JstreeModel.prototype.setOptions = function (options) {
            this._options = options;
            this._fakeRoot && this._fakeRoot._buildIndex(this._options);
            this.fire("setOptions", {
                data: options
            });
            return this;
        };
        JstreeModel.prototype.setSelectionFilter = function (selectionFilter) {
            this._selectionFilter = selectionFilter;
            if (this._selectionFilter == null) {
                this._selectionFilter = function () { return true; };
            }
            if (this._options.multiple) {
                this._checkFilter = function () { return true; };
            }
            else {
                this._checkFilter = this._selectionFilter;
            }
            return this;
        };
        JstreeModel.prototype.getIdsByLevel = function (level) {
            return _(this._nodes).filter(function (node) { return node.level === level; }).map("id").value();
        };
        return JstreeModel;
    }(stcontrols.EventSource));
    stcontrols.JstreeModel = JstreeModel;
    stcontrols.module.value("JstreeModel", JstreeModel);
})(stcontrols || (stcontrols = {}));
var stcontrols;
(function (stcontrols) {
    stcontrols.module
        .directive("jstree", [
        "$q",
        "$timeout",
        function ($q, $timeout) {
            return {
                templateUrl: "jstree/jstree.directive.html",
                scope: {
                    model: "="
                },
                link: function (scope, element, attrs) {
                    scope.searchText = "";
                    scope.search = function () { return instance.search(scope.searchText); };
                    var $tree;
                    var instance;
                    var defer;
                    var $treeContainer = element.find(".tree-container");
                    var model = scope.model;
                    function getSelected() {
                        var selectedNodes = instance.get_selected(true);
                        var filtered = _.filter(selectedNodes, function (node) { return model._selectionFilter(node.original, node, instance); });
                        return _.map(filtered, 'id');
                    }
                    function ensure(action) {
                        if (defer) {
                            defer.promise.then(action);
                        }
                    }
                    function initTree() {
                        if ($tree) {
                            $tree.remove();
                        }
                        defer = $q.defer();
                        $tree = $("<div>").appendTo($treeContainer)
                            .bind('loaded.jstree', function () {
                            instance = $tree.jstree(true);
                            setSelectedFromModel();
                            setDisabledFromModel();
                            setHiddenFromModel();
                            defer.resolve();
                        }).bind('changed.jstree', function () {
                            var selectedIds = getSelected();
                            model.setSelectedIds(selectedIds, true);
                            $timeout();
                        });
                        var treeOptions = {
                            core: {
                                data: model._fakeRoot.children,
                                check_callback: true,
                                multiple: !!model._options.multiple,
                                expand_selected_onload: false
                            },
                            plugins: [
                                "search",
                                "conditionalselect"
                            ],
                            search: {
                                show_only_matches: true,
                                show_only_matches_children: true
                            },
                            conditionalselect: function (node, event) { return model._checkFilter(node.original, node, instance); }
                        };
                        if (model._options.multiple) {
                            treeOptions.plugins.push('checkbox');
                            treeOptions.checkbox = {
                                visible: true,
                                whole_node: true,
                                tie_selection: true,
                                keep_selected_style: false
                            };
                        }
                        $tree.jstree(treeOptions);
                    }
                    function setSelectedFromModel() {
                        if (instance) {
                            instance.deselect_all(true);
                            instance.select_node(model.getSelectedIds(), true, !!model._options.collapseSelected);
                        }
                    }
                    function setDisabledFromModel() {
                        if (instance) {
                            instance.disable_node(model.getDisabledIds());
                        }
                    }
                    function setHiddenFromModel() {
                        if (instance) {
                            instance.show_all(false);
                            instance.hide_node(model.getHiddenIds(), false);
                        }
                    }
                    model.onAll(["setItems", "setOptions"], function (event) { return initTree(); })
                        .on("setSelected", function (event) {
                        if (!event.fromView) {
                            ensure(setSelectedFromModel);
                        }
                    }).on("setDisabled", function (event) {
                        ensure(setDisabledFromModel);
                    }).on("setHidden", function (event) { return ensure(setHiddenFromModel); });
                }
            };
        }
    ]);
})(stcontrols || (stcontrols = {}));
var stcontrols;
(function (stcontrols) {
    var NodeFilterFactory = (function () {
        function NodeFilterFactory() {
        }
        NodeFilterFactory.prototype.isLeaf = function () {
            return function (node) { return node.isLeaf; };
        };
        NodeFilterFactory.prototype.isNotLeaf = function () {
            return function (node) { return !node.isLeaf; };
        };
        NodeFilterFactory.prototype.parentNotSelected = function () {
            return function (node, libNode, jstreeInstance) {
                var parentLibNode = jstreeInstance.get_node(libNode.parent);
                if (!parentLibNode) {
                    return true;
                }
                return !parentLibNode.state.selected;
            };
        };
        NodeFilterFactory.prototype.isInLevel = function (level) {
            return function (node) { return node.level == level; };
        };
        NodeFilterFactory.prototype.hasId = function (ids) {
            var idIndex = _.keyBy(ids, function (id) { return id; });
            return function (node) { return idIndex[node.id] !== undefined; };
        };
        NodeFilterFactory.prototype.trueForAll = function (filters) {
            return function (node, libNode, jstreeInstance) {
                var keep = true;
                for (var i = 0; i < filters.length; i++) {
                    var filter = filters[i];
                    keep = filter(node, libNode, jstreeInstance);
                    if (!keep)
                        break;
                }
                return keep;
            };
        };
        return NodeFilterFactory;
    }());
    stcontrols.nodeFilterFactory = new NodeFilterFactory();
    stcontrols.module.value("nodeFilterFactory", stcontrols.nodeFilterFactory);
})(stcontrols || (stcontrols = {}));

angular.module('stcontrols').run(['$templateCache', function($templateCache) {$templateCache.put('jstree/jstree-dropdown.directive.html','<div class="btn-group" uib-dropdown auto-close="outsideClick">\r\n    <button type="button" class="btn btn-default" ng-disabled="ngDisabled" uib-dropdown-toggle>\r\n                {{selectionText}} <span class="caret"></span>\r\n            </button>\r\n    <div class="dropdown-menu" uib-dropdown-menu>\r\n        <jstree model="model" options="options"></jstree>\r\n    </div>\r\n</div>');
$templateCache.put('jstree/jstree.directive.html','<div class="st-jstree">\r\n    <div class="tree-search-container">\r\n        <input type="search" placeholder="Search inside tree" ng-model="searchText" ng-change="search()" class="form-control" />\r\n    </div>\r\n    <div class="tree-container"></div>\r\n</div>\r\n');}]);